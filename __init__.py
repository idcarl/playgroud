from flask import Flask
from flask_mongoengine import MongoEngine
from pymongo import *
from config import Config

app = Flask("CARL_WebApp",
            static_folder='/Users/jingwang/OneDrive - rwth-aachen.de/Arbeit/CARL_WebApp_for_Mac/The_Real_CARL/static',
            template_folder='/Users/jingwang/OneDrive - rwth-aachen.de/Arbeit/CARL_WebApp_for_Mac/The_Real_CARL'
                            '/templates')
app.config.from_object(Config)
db = MongoEngine()
db.init_app(app)
Mongo_Client = MongoClient('mongodb://127.0.0.1:27017').xCluster
Collection_User = Mongo_Client['webapp_user']
Collection_SollChemzusetzung = Mongo_Client['Soll_Chemische_Zusammensezung']
from Application import routes